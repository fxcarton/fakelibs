# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit multilib-minimal

DESCRIPTION="[FAKE] Fake libdbus.so"
HOMEPAGE="https://dbus.freedesktop.org/"
SRC_URI="https://dbus.freedesktop.org/releases/dbus/${P}.tar.gz"

LICENSE="|| ( AFL-2.1 GPL-2 )"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm ~arm64"
IUSE=""

BDEPEND=""
COMMON_DEPEND=""
DEPEND=""
RDEPEND=""

get_headers() {
	sed -n ':o;/^[a-zA-Z_]*include_HEADERS[\t ]*=[\t ]*\\$/{:r;n;/^$/bo;s/^[\t ]*//;s/\\$//;s/[\t ]*$//;p;br}' Makefile.am
}

get_sover() {
	export LT_CURRENT="$(sed -n 's/^LT_CURRENT=//p' < "${S}/configure.ac")"
	export LT_AGE="$(sed -n 's/^LT_AGE=//p' < "${S}/configure.ac")"
	export LT_REVISION="$(sed -n 's/^LT_REVISION=//p' < "${S}/configure.ac")"
	export SOVERSION="$((LT_CURRENT - LT_AGE))"
}

src_prepare() {
	eapply_user
	get_sover
	cd dbus || die
	sed -e "s/@SOVERSION@/${SOVERSION}/g" \
		-e "s/@DBUS_VERSION@/${PV}/g" \
		Version.in > Version || die
	sed -e 's/@DBUS_INT\([0-9]*\)_TYPE@/int\1_t/g' \
		-e 's/@DBUS_\(U*\)INT64_CONSTANT@/((\1int64_t)val)/g;s/(U\(int64_t)\)/(u\1/g' \
		-e 's/unsigned \(int[0-9]*_t\)/u\1/g' \
		-e "s/@DBUS_MAJOR_VERSION@/$(ver_cut 1)/g" \
		-e "s/@DBUS_MINOR_VERSION@/$(ver_cut 2)/g" \
		-e "s/@DBUS_MICRO_VERSION@/$(ver_cut 3)/g" \
		-e "s/@DBUS_VERSION@/$(ver_cut 1-3)/g" \
		-e 's/^DBUS_BEGIN_DECLS$/#include <stdint.h>\n&/' \
		dbus-arch-deps.h.in > dbus-arch-deps.h || die
	get_headers | xargs cat | # with public headers
		sed ':r;/[,(] *$/{N;s/\([,(]\) *\n/\1 /;br}' | # merge lines ending with a comma/parenthesis with the next one (removing new line)
		sed 'N;s/\n\(_*dbus_\)/ \1/;h;s/\n.*//;p;x;D' | # if line starts with dbus_, replace previous new line with space
		sed 's/[ \t]*DBUS_MALLOC//g;s/[ \t]*DBUS_ALLOC_SIZE([0-9]*)//g;/^$/d' | # remove crap
		sed '/^DBUS_EXPORT$/{N;s/\n/ /}' | # if line only has DBUS_EXPORT, replace next new line with space
		grep 'DBUS_EXPORT' | # now that we ensured function declarations are on single lines, extract them
		sed '/^#/d;/@def DBUS_EXPORT/d' | # remove crap
		sed 's/^\(DBUS_EXPORT  *void  *_*dbus.*)\) *; *$/\1{}/' | # "implement" void functions (doing nothing)
		sed 's/) *; *$/){return 0;}/' | # "implement" non-void functions (return 0)
		{
			printf '#include "%s"\n' dbus.h; # includes
			printf '#undef _DBUS_GNUC_PRINTF\n#define _DBUS_GNUC_PRINTF(a, b)\n'
			cat; # implementation
		} > fakeimplem.c || die
}

multilib_src_configure() {
	:
}

echoit() { printf '%s\n' "$*"; "$@"; }

multilib_src_compile() {
	get_sover
	echoit $(tc-getCC) $(get_abi_CFLAGS) $CFLAGS $("$(tc-getPKG_CONFIG)" --cflags glib-2.0) -I"${S}" -fPIC -c "${S}/dbus/fakeimplem.c" -o fakeimplem.o || die
	echoit $(tc-getCC) $(get_abi_CFLAGS) $CFLAGS $LDFLAGS -Wl,--version-script="${S}/dbus/Version" -Wl,-soname,"lib${PN}-1.so.$SOVERSION" -fPIC -shared fakeimplem.o -o libdbus-1.so || die
	sed -e 's:@prefix@:/usr:g' \
		-e 's:@pkgconfig_prefix@:${original_prefix}:g' \
		-e 's:@exec_prefix@:${prefix}:g' \
		-e 's:@bindir@:${exec_prefix}/bin:g' \
		-e "s:@libdir@:/usr/$(get_abi_LIBDIR):g" \
		-e 's:@includedir@:${prefix}/include:g' \
		-e 's:@datarootdir@:${prefix}/share:g' \
		-e 's:@datadir@:${prefix}/share:g' \
		-e 's:@sysconfdir@:/etc:g' \
		-e 's:@dbus_daemondir@:${bindir}:g' \
		-e "s:@VERSION@:${PV}:g" \
		-e 's:@[^@]*@::g' \
		"${S}/dbus-1.pc.in" > dbus-1.pc || die
}

multilib_src_install() {
	get_sover
	newlib.so libdbus-1.so "libdbus-1.so.$SOVERSION.$LT_AGE.$LT_REVISION"
	dosym "libdbus-1.so.$SOVERSION.$LT_AGE.$LT_REVISION" "usr/$(get_abi_LIBDIR)/libdbus-1.so.$SOVERSION"
	dosym "libdbus-1.so.$SOVERSION.$LT_AGE.$LT_REVISION" "usr/$(get_abi_LIBDIR)/libdbus-1.so"
	insinto "usr/$(get_abi_LIBDIR)/pkgconfig"
	doins dbus-1.pc
}

multilib_src_install_all() {
	insinto usr/include/dbus
	cd dbus || die
	doins $(get_headers)
}
