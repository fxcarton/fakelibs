# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit gnome.org toolchain-funcs multilib multilib-minimal

DESCRIPTION="[FAKE] Gtk module for bridging AT-SPI to Atk"
HOMEPAGE="https://wiki.gnome.org/Accessibility"

LICENSE="LGPL-2+"
SLOT="2"
KEYWORDS="~amd64 ~x86 ~arm ~arm64"
IUSE=""

DEPEND="${RDEPEND}"
RDEPEND="
	>=dev-libs/glib-2.32:2[${MULTILIB_USEDEP}]
	>=app-accessibility/at-spi2-core-2.31.2[${MULTILIB_USEDEP}]
"
BDEPEND=""

src_prepare() {
	eapply_user
	cd atk-adaptor || die
	cat atk-bridge.h | # with all headers
		sed ':r;/, *$/{N;s/, *\n/, /;br}' | # merge lines ending with a comma with the next one (removing new line)
		sed 'N;s/\n\(_*atk_bridge_\)/ \1/;h;s/\n.*//;p;x;D' | # if line starts with atk_bridge_, replace previous new line with space
		grep '[^#]atk_bridge_' | # now that we ensured function declarations are on single lines, extract them
		grep -v '^#define' | # filter defines
		sed 's/^\(void  *_*atk_bridge.*)\) *; *$/\1{}/' | # "implement" void functions (doing nothing)
		sed 's/) *; *$/){return 0;}/' | # "implement" non-void functions (return 0)
		{
			printf '#include "%s"\n' glib.h atk-bridge.h; # includes
			printf '#%s G_GNUC_CONST\n' undef define; # fix prototypes
			cat; # implementation
		} > fakeimplem.c || die
}

multilib_src_configure() {
	:
}

echoit() { printf '%s\n' "$*"; "$@"; }

multilib_src_compile() {
	soversion="$(sed -n 's/.*soversion *= *\(.*\)$/\1/p' "${S}/meson.build")" || die
	echoit $(tc-getCC) $(get_abi_CFLAGS) $CFLAGS $("$(tc-getPKG_CONFIG)" --cflags glib-2.0) -I"${S}" -fPIC -c "${S}/atk-adaptor/fakeimplem.c" -o fakeimplem.o || die
	echoit $(tc-getCC) $(get_abi_CFLAGS) $CFLAGS $LDFLAGS -Wl,-soname,"libatk-bridge-2.0.so.$soversion" -fPIC -shared fakeimplem.o -o libatk-bridge-2.0.so || die
	printf '%s=%s\n' \
		prefix "${EPREFIX}/usr" \
		libdir "\${prefix}/$(get_abi_LIBDIR)" \
		includedir "\${prefix}/include" \
		> atk-bridge-2.0.pc
	printf '%s: %s\n' \
		Name atk-bridge-2.0 \
		Description "[FAKE] ATK/D-Bus Bridge" \
		Version "${PV}" \
		Libs '-L${libdir} -latk-bridge-2.0' \
		Cflags '-I${includedir}/at-spi2-atk/2.0' \
		>> atk-bridge-2.0.pc
}

multilib_src_test() {
	:
}

multilib_src_install() {
	soversion="$(sed -n 's/.*soversion *= *\(.*\)$/\1/p' "${S}/meson.build")" || die
	libversion="$(sed -n "s/.*libversion *= *'\\(.*\\)'\$/\\1/p" "${S}/meson.build")" || die
	newlib.so libatk-bridge-2.0.so "libatk-bridge-2.0.so.$libversion"
	dosym "libatk-bridge-2.0.so.$libversion" "usr/$(get_abi_LIBDIR)/libatk-bridge-2.0.so.$soversion"
	dosym "libatk-bridge-2.0.so.$libversion" "usr/$(get_abi_LIBDIR)/libatk-bridge-2.0.so"
	insinto "usr/$(get_abi_LIBDIR)/pkgconfig"
	doins atk-bridge-2.0.pc
}

multilib_src_install_all() {
	insinto usr/include/at-spi2-atk/2.0/
	doins atk-adaptor/atk-bridge.h
}
