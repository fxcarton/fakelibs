# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit gnome.org toolchain-funcs multilib multilib-minimal

DESCRIPTION="[FAKE] D-Bus accessibility specifications and registration daemon"
HOMEPAGE="https://wiki.gnome.org/Accessibility"

LICENSE="LGPL-2+"
SLOT="2"
KEYWORDS="~amd64 ~x86 ~arm ~arm64"
IUSE=""

DEPEND="${RDEPEND}"
RDEPEND=">=dev-libs/glib-2.36:2[${MULTILIB_USEDEP}]"
BDEPEND=""

src_prepare() {
	eapply_user
	cd atspi || die
	rm *-private.h || die
	glib-mkenums --template atspi-enum-types.h.template atspi-constants.h atspi-types.h > atspi-enum-types.h || die
	mkdir ../dbus || die
	printf '#define %s void\n' DBusConnection DBusServer DBusMessageIter > ../dbus/dbus.h || die
	cat *.h | # with all headers
		sed ':r;/, *$/{N;s/, *\n/, /;br}' | # merge lines ending with a comma with the next one (removing new line)
		sed 'N;s/\n\(_*atspi_\)/ \1/;h;s/\n.*//;p;x;D' | # if line starts with atspi_, replace previous new line with space
		grep '[^#]atspi_' | # now that we ensured function declarations are on single lines, extract them
		grep -v '^#define' | # filter defines
		grep -v atspi_editable_text_set_attributes | # remove that, it's enclosed in '#if 0'
		sed 's/^\(void  *_*atspi.*)\) *; *$/\1{}/' | # "implement" void functions (doing nothing)
		sed 's/) *; *$/){return 0;}/' | # "implement" non-void functions (return 0)
		{
			printf '#include "%s"\n' glib.h *.h; # includes
			printf '#%s G_GNUC_CONST\n' undef define; # fix prototypes
			cat; # implementation
		} > fakeimplem.c || die
}

multilib_src_configure() {
	:
}

echoit() { printf '%s\n' "$*"; "$@"; }

multilib_src_compile() {
	soversion="$(sed -n "s/^soversion *= *'\\(.*\\)'$/\\1/p" "${S}/meson.build")" || die
	echoit $(tc-getCC) $(get_abi_CFLAGS) $CFLAGS $("$(tc-getPKG_CONFIG)" --cflags glib-2.0) -I"${S}" -fPIC -c "${S}/atspi/fakeimplem.c" -o fakeimplem.o || die
	echoit $(tc-getCC) $(get_abi_CFLAGS) $CFLAGS $LDFLAGS -Wl,-soname,"libatspi.so.$(cut -d. -f1 <<< "$soversion")" -fPIC -shared fakeimplem.o -o libatspi.so || die
	printf '%s=%s\n' \
		prefix "${EPREFIX}/usr" \
		libdir "\${prefix}/$(get_abi_LIBDIR)" \
		includedir "\${prefix}/include" \
		> atspi-2.pc
	printf '%s: %s\n' \
		Name atspi \
		Description "[FAKE] Accessibility Technology software library" \
		Version "${PV}" \
		Requires glib-2.0 \
		Libs '-L${libdir} -latspi' \
		Cflags '-I${includedir}/at-spi-2.0' \
		>> atspi-2.pc
}

multilib_src_test() {
	:
}

multilib_src_install() {
	soversion="$(sed -n "s/^soversion *= *'\\(.*\\)'$/\\1/p" "${S}/meson.build")" || die
	newlib.so libatspi.so "libatspi.so.$soversion"
	dosym "libatspi.so.$soversion" "usr/$(get_abi_LIBDIR)/libatspi.so.$(cut -d. -f1 <<< "$soversion")"
	dosym "libatspi.so.$soversion" "usr/$(get_abi_LIBDIR)/libatspi.so"
	insinto "usr/$(get_abi_LIBDIR)/pkgconfig"
	doins atspi-2.pc
}

multilib_src_install_all() {
	insinto usr/include/at-spi-2.0/atspi
	doins atspi/*.h
	insinto usr/include/at-spi-2.0/dbus
	doins dbus/dbus.h
}
